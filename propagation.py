import numpy as np
import scipy
from scipy import signal
import pylab
from scipy.io import wavfile
import matplotlib.pyplot as plt
from matplotlib import animation

frequency 	= 10					#Hertz
#frequency 	= 50					#cycles per second (Hertz)
omega 		= 2 * np.pi * frequency	#Angular frequency
amplitude 	= 1
phase     	= 45 					#degrees

c = 340#propagation speed (m s^-1)
wavelength = c/frequency				#meters
wavenumber = 2 * (np.pi /wavelength) #radians per meter

R = 0.6

# First set up the figure, the axis, and the plot element we want to animate
fig = plt.figure()
ax = plt.axes(xlim=(0, 100), ylim=(-2, 2))
line, = ax.plot([], [], lw=2, color="green")
line2, = ax.plot([], [], lw=2, color="blue")
line3, = ax.plot([], [], lw=5, color="red")


#ax.set_axis_bgcolor('blue')

timetext = ax.text(0.5, 50, '')

def forward_propagation(R, x, t):
	global wavenumber
	global omega
	return R * np.cos(((omega * t) - (wavenumber * x)))

def backward_propagation(R, x, t):
	global wavenumber
	global omega
	return (1-R) * np.cos((omega *  t) + (wavenumber * x))
	
def combined_wave(x, t):
	return forward_propagation(x, t) + backward_propagation(x, t)


# initialization function: plot the background of each frame
def init():
	line.set_data([], [])
	line2.set_data([], [])
	line3.set_data([], [])

	return [line, line2, line3]

# animation function.  This is called sequentially
def animate(i):
	global ax
	global R

	p = np.linspace(0, 100, num=10000)

	fw = forward_propagation(R, p, i/1000)
	bw = backward_propagation(R, p, i/1000)

	combined = []

#	for j in range(len(p) * 2):
#		combined.append([])
#		for l in p:
#			combined[j].append(combined_wave(l, i))

	x = p
	y = p

	timetext.set_text(i)
	
	line.set_data(p, bw)
	line2.set_data(p, fw)
	line3.set_data(p, bw + fw)
	return tuple([line, line2, line3]) + (timetext,)


#TODO - Implement somehting like this for the heatmap
# import numpy as np
# import numpy.random
# import matplotlib.pyplot as plt

# # Generate some test data
# x = np.random.randn(8873)
# y = np.random.randn(8873)

# heatmap, xedges, yedges = np.histogram2d(x, y, bins=50)
# extent = [xedges[0], xedges[-1], yedges[0], yedges[-1]]

# plt.clf()
# plt.imshow(heatmap, extent=extent)
# plt.show()

# call the animator.  blit=True means only re-draw the parts that have changed.
anim = animation.FuncAnimation(fig, animate, init_func=init, frames=1000, interval=50, blit=True)

# save the animation as an mp4.  This requires ffmpeg or mencoder to be
# installed.  The extra_args ensure that the x264 codec is used, so that
# the video can be embedded in html5.  You may need to adjust this for
# your system: for more information, see
# http://matplotlib.sourceforge.net/api/animation_api.html

#print ("Saving animation as MPEG4")
#anim.save('sinusoid.mp4', writer='ffmpeg', fps=30, extra_args=['-vcodec', 'libx264'])

plt.title("Wave Propogation")
plt.show()